package com.devcamp.j52_javasic.s50;

import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;

public class Order {
    int id; // id của order
    String customerName; // tên khách hàng
    long price;// tổng giá tiền
    LocalDate orderDate; // ngày thực hiện order
    boolean confirm; // đã xác nhận hay chưa?
    String[] items; // danh sách mặt hàng đã mua
    public Order(int id, String customerName, long price, LocalDate myLocalDate, boolean confirm, String[] items) {
        this.id = id;
        this.customerName = customerName;
        this.price = price;
        this.orderDate = myLocalDate;
        this.confirm = confirm;
        this.items = items;
    }
    /**
     * @param id
     */
    public Order(int id) {
        this.id = id;
        this.customerName = "Resource";
        this.price = 20000;
       // this.orderDate = LocalDate.of(2020, Month.MARCH, 11);
        this.confirm = true;
        this.items = new String[] {"parasol"};
    }
    public Order(int id, String customerName) {
        this.id = id;
        this.customerName = customerName;
        this.price = 20000;
        this.items = new String[] {"parasol"};

    }
    public Order(int id, String customerName, long price) {
        this.id = id;
        this.customerName = customerName;
        this.price = price;
        this.items = new String[] {"parasol"};
    }
    
    public String toString(){
        return "Order information: " + System.lineSeparator()
         + "id: " + id + System.lineSeparator()
         + "customer name: " + customerName + System.lineSeparator()
         + "price: " + price + System.lineSeparator()
         + "order date: " + orderDate + System.lineSeparator()
         + "confirm: " + confirm + System.lineSeparator()
         + "item: " + items[0] + ", " + items[1];
    }
    /**
     * @param args
     * @throws Exception
     */
    public static void main(String[] args) throws Exception {
        ArrayList<Order> newOrder = new ArrayList<>();
        LocalDate myLocalDate = LocalDate.of(2020, Month.NOVEMBER, 11);
        //tạo 4 order 
        Order newOrder1 = new Order(1, "OHMM", 5000,myLocalDate , true, new String[] {"table","chair"});
        Order newOrder2 = new Order(2, "Xtra", 10000);
        Order newOrder3 = new Order(3, "Xtra");
        Order newOrder4 = new Order(4);
        
        newOrder.add(newOrder1);
        newOrder.add(newOrder2);
        newOrder.add(newOrder3);
        newOrder.add(newOrder4);
    
        for (int i = 0; i< newOrder.size();i++){
            System.out.println(newOrder.get(i).toString());
        }
    } 
    
}
